package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"regexp"
	"errors"
	"net/url"
	"strings"
)

const (
	BROWSER_SIGNATURE = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/34.0.1847.116 Chrome/34.0.1847.116 Safari/537.37"
)

func main() {
	/* ASP.NET_SessionId=reglggmwhravenwg2srsteu5xlnv; rateCodes=%7B%22en-GB%22%3A%22FOP%22%7D; s_cm=Typed%2FBookmarkedTyped%2FBookmarkedundefined; drcsess=56608262052; campPath=n%2Fa; s_ev44=%5B%5B'Direct'%2C'1410288614239'%5D%2C%5B'Direct'%2C'1410288614710'%5D%2C%5B'Direct'%2C'1410289958407'%5D%2C%5B'Direct'%2C'1410289960858'%5D%2C%5B'Direct'%2C'1410289962918'%5D%5D; s_cc=true; New_Location=regl; prevPage=website%3A%20en%3A%20booking%3A%20select%3A%20none; s_cm_dl=1; s_sq=ryanairprod%3D%2526pid%253Dwebsite%25253A%252520en%25253A%252520booking%25253A%252520select%25253A%252520none%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.bookryanair.com%25252FSkySales%25252FBooking.aspx%25253Fculture%25253Den-gb%252526lc%25253Den-gb%2526ot%253DA
	*/
	//cookie := "reglggmwhravenwg2srsteu5xlnv"
	cookie, viewState, err := fetchCookie()
	fmt.Println(cookie)
	fmt.Println(viewState)
	fmt.Println(err)
	if err == nil {
		setSearch(cookie, viewState)
	}
	fmt.Println(cookie)
	fetchNextWeek(cookie)
}

func setSearch(cookie, viewState string) {
	// Set the cookie by opening the search empty the first time
	client := &http.Client{}

	/*
	ireq, err := http.NewRequest("POST", "https://www.bookryanair.com/SkySales/Search.aspx", nil)
	ireq.Header.Add("Cookie", fmt.Sprintf("ASP.NET_SessionId=%s; New_Location=regl", cookie))
	ireq.Header.Add("Referer", "https://www.bookryanair.com/SkySales/Booking.aspx?culture=en-gb&lc=en-gb")
	ireq.Header.Add("User-Agent", BROWSER_SIGNATURE)
	iresp, err := client.Do(ireq)
	if err == nil {
		ibodyText, err := ioutil.ReadAll(iresp.Body)
		iresp.Body.Close()
		fmt.Printf("All: %s\n Body: %s, Err: %s\n", iresp, ibodyText, err)
	}
	*/

	// Make the first call to set the actual source/destination airports
	requestUrl := "https://www.bookryanair.com/SkySales/Select.aspx"

	v := url.Values{}
	v.Set("__EVENTTARGET", "SearchInputSelect$ButtonSubmit")
	v.Set("__EVENTARGUMENT", "")
	v.Set("__VIEWSTATE", viewState)
	v.Set("formaction", "Select.aspx")
	v.Set("errorlist", "")
	v.Set("SearchInputSelect$IsFlexible", "on")
	v.Set("SearchInputSelect$TripType", "OneWay")
	v.Set("SearchInputSelect$Orig", "STN")
	v.Set("SearchInputSelect$Dest", "PDV")
	v.Set("SearchInputSelect$DeptDate", "10/09/2014")
	v.Set("SearchInputSelect$RetDate", "15/09/2014")
	v.Set("SearchInputSelect$PaxTypeADT", "1")
	v.Set("SearchInputSelect$PaxTypeCHD", "0")
	v.Set("SearchInputSelect$PaxTypeINFANT", "0")

	req, err := http.NewRequest("POST", requestUrl, strings.NewReader(v.Encode()))

	req.Header.Add("Cookie", fmt.Sprintf("rateCodes=%7B%22en-GB%22%3A%22FOP%22%7D; ASP.NET_SessionId=%s; s_cm=Typed%2FBookmarkedTyped%2FBookmarkedundefined; campPath=n%2Fa; s_cc=true; New_Location=regl; prevPage=website%3A%20en%3A%20booking%3A%20select%3A%20none; s_cm_dl=1; s_sq=ryanairprod%3D%2526pid%253Dwebsite%25253A%252520en%25253A%252520booking%25253A%252520select%25253A%252520none%2526pidt%253D1%2526oid%253DSEARCH%2526oidt%253D3%2526ot%253DSUBMIT", cookie))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("Origin", "https://www.bookryanair.com")
	req.Header.Add("Referer", "https://www.bookryanair.com/SkySales/Booking.aspx?culture=en-gb&lc=en-gb")
	// keep for referrence so we don't display the scraper http client libs
	req.Header.Add("User-Agent", BROWSER_SIGNATURE)
	req.Header.Add("X-Requested-With", "XMLHttpRequest")

	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	bodyText, err := ioutil.ReadAll(resp.Body)
	fmt.Printf("All: %s\n Body: %s, Err: %s\n", resp, bodyText, err)
	//fmt.Printf("Err: %s\n", err)
}

func fetchCookie() (cookie, viewState string, err error) {
	requestUrl := "https://www.bookryanair.com/SkySales/Booking.aspx?culture=en-gb&lc=en-gb"
	client := &http.Client{}
	req, err := http.NewRequest("GET", requestUrl, nil)
	req.Header.Add("User-Agent", BROWSER_SIGNATURE)
	resp, err := client.Do(req)

	foundCookie := false
	for _, c := range resp.Cookies() {
		if c.Name == "ASP.NET_SessionId" {
			cookie = c.Value
			foundCookie = true
			break
		}
	}
	if !foundCookie {
		err = errors.New("Didn't receive a session cookie")
		return
	}

	//bodyTextString := "\ngaage id=\"__VIEWSTATE\" value=\"/wEPDwUBMGRkwiC+diO7YHrN9X8OU5GYY7FHcEU=\" more garbage\n\nmorecrap"
	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = errors.New("Error reading the response body")
		return
	}
	bodyTextString := string(bodyText)

	var r = regexp.MustCompile(`(?m)^.*id="__VIEWSTATE" value="(.*=)"><div id="main.*$`)
	matches := r.FindAllStringSubmatch(bodyTextString,2)
	if len(matches) != 1 || len(matches[0]) != 2 {
		err = errors.New("Could not locate the VIEWSTATE id")
		return
	}
	viewState = matches[0][1]

	return
}

func fetchNextWeek(cookie string) {
	requestUrl := "https://www.bookryanair.com/SkySales/Select-resource.aspx?action=SelectInput$LinkButtonNext1"
	client := &http.Client{}

	req, err := http.NewRequest("POST", requestUrl, nil)
	req.Header.Add("Accept", "application/json, text/plain, */*")

	// Not necessary
	//req.Header.Add("Content-Length", "0")
	req.Header.Add("Cookie", fmt.Sprintf("ASP.NET_SessionId=%s; rateCodes=%7B%22en-GB%22%3A%22FOP%22%7D; s_cc=true; s_cm=Typed%2FBookmarkedTyped%2FBookmarkedundefined; campPath=n%2Fa; New_Location=regl; prevPage=website%3A%20en%3A%20booking%3A%20select%3A%20none; s_cm_dl=1; s_sq=%5B%5BB%5D%5D", cookie))

	req.Header.Add("Header", "www.bookryanair.com")
	req.Header.Add("Origin", "https://www.bookryanair.com")
	req.Header.Add("Referer", "https://www.bookryanair.com/SkySales/Booking.aspx?culture=en-gb&lc=en-gb")
	// keep for referrence so we don't display the scraper http client libs
	req.Header.Add("User-Agent", BROWSER_SIGNATURE)

	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	bodyText, err := ioutil.ReadAll(resp.Body)
	fmt.Printf("All: %s\n Body: %s, Err: %s\n", resp, bodyText, err)
}
