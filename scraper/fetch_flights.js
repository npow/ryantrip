var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));

var elasticsearch = require('elasticsearch');
var client = elasticsearch.Client({ host: process.env.ELASTICSEARCH_URL || 'localhost:9200' });
var timestamp = new Date().toISOString();
var CURRENT_INDEX_STR = 'ryantrip_current';
var INDEX = 'ryantrip_' + timestamp.toLowerCase();
var TYPE = 'flight';

var g_visited = {};

function createIndex() {
  return client.indices.create({
    index: INDEX
  });
}

function updateAliases() {
  return client.indices.updateAliases({
    body: {
      actions: [
        { remove: { index: '_all', alias: CURRENT_INDEX_STR } },
        { add: { index: INDEX, alias: CURRENT_INDEX_STR } },
      ]
    }
  });
}

// returns yyyy-mm-dd
function formatDate(date) {
  return date.toISOString().substring(0, 10);
}

function fetchFlightsForAirport(startDate, endDate, airportCode) {
  var url = 'http://www.ryanair.com/en/api/2/flights/from/' + airportCode + '/' + formatDate(startDate) + '/' + formatDate(endDate) + '/250/unique/?limit=1000';
  console.log(url);

  g_visited[airportCode] = true;
  return request.getAsync(url).get(1).then(function (response) {
    response = JSON.parse(response);
    var airportCodes = [];
    return Promise.all(response.flights.map(function (flight) {
      var targetAirportCode = flight.outbound.airportTo.iataCode;
      if (!g_visited[targetAirportCode] && airportCodes.indexOf(targetAirportCode) === -1) {
        airportCodes.push(targetAirportCode);
      }
      return client.index({ index: INDEX, type: TYPE, body: flight });
    })).then(function () {
      return Promise.all(airportCodes.map(function (x) {
        return fetchFlightsForAirport(startDate, endDate, x);
      }));
    });
  }).catch(function (err) {
    console.log(err);
  });
}

function fetchFlights(startDate, endDate) {
  return fetchFlightsForAirport(startDate, endDate, 'STN');
}

function main() {
  var startDate = new Date();
  var endDate = new Date();
  endDate.setMonth(endDate.getMonth() + 9); // 9 months from today

  createIndex().then(function () {
    return fetchFlights(startDate, endDate);
  }).then(function () {
    return updateAliases();
  }).then(function () {
    client.close()
    console.log("DONE");
  }).error(function (err) {
    console.log(err);
  });
}

main();
