var elasticsearch = require('elasticsearch');
var client = elasticsearch.Client({ host: process.env.ELASTICSEARCH_URL || 'localhost:9200' });

function toGMT(x) {
  x = new Date(x);
  return new Date(x.valueOf() + x.getTimezoneOffset() * 60000);
}

function getNextLeg(flightHash, startDate, airportCode, visited) {
  var flights = flightHash[airportCode];
  if (!flights) {
    return;
  }
  var result;
  for (var i = 0, l = flights.length; i < l; ++i) {
    var flight = flights[i];
    if (visited.indexOf(flight.outbound.airportTo.iataCode) === -1 &&
        flight.outbound.dateFrom > startDate &&
//        flight.outbound.dateFrom - startDate < 7*86400*1000 &&
        (!result || flight.outbound.price.value < result.outbound.price.value)) {
      result = flight;
    }
  }
  return result;
}

function calculateItinerary(flights, startDate, airportCode) {
  var flightHash = {};
  flights.forEach(function (flight) {
    var fromAirportCode = flight.outbound.airportFrom.iataCode;
    flight.outbound.dateFrom = toGMT(flight.outbound.dateFrom);
    flight.outbound.dateTo = toGMT(flight.outbound.dateTo);
    flightHash[fromAirportCode] = flightHash[fromAirportCode] || [];
    flightHash[fromAirportCode].push(flight);
  });

  var itinerary = [];
  var visited = [];
  while (true) {
    var flight = getNextLeg(flightHash, startDate, airportCode, visited);
    if (!flight) break;
    visited.push(airportCode);
    itinerary.push(flight);
    startDate = flight.outbound.dateFrom;
    startDate.setDate(startDate.getDate() + 3); // buffer
    airportCode = flight.outbound.airportTo.iataCode;
  }
  return itinerary;
}

var airports = [
  'BLQ',
  'PSA',
  'OPO',
  'CIA',
  'MAD',
  'PMI',
  'VLC',
  'AGP',
  'LEI',
  'SCQ',
  'SVQ',
  'KRK',
  'SKG',
  'EFL',
  'FAO',
  'CFU',
  'REU',
  'MRS',
  'NUE',
  'FMM',
  'OSI',
  'CGN',
  'WMI',
  'ZAZ',
  'BGY',
  'BCN',
  'LIS',
  'GOA',
  'TRS',
  'PSR',
  'WRO',
  'LNZ',
  'PMF',
  'AOI',
  'STN',
  'LDY',
  'NOC',
  'SNN',
  'ORK',
  'GDN',
  'EDI',
  'POZ',
  'SFT',
  'LRH',
  'NRN',
  'DUB',
  'DTM',
  'BOD',
  'BRE',
  'SXB',
  'RYG',
  'TRF',
  'GRO',
  'EIN',
  'ZAD',
  'EGC',
  'TUF',
  'RDZ',
  'LIG',
  'NYO',
  'GSE',
  'HAU',
  'OTP',
  'TRN',
  'BLL',
  'FKB',
  'HHN',
  'CCF',
  'KTW',
  'BZG',
  'SDR',
  'AHO',
  'RZE',
  'LCJ',
  'SUF',
  'TMP',
  'TSF',
  'BTS',
  'VNO',
  'RHO',
  'CHQ',
  'LPA',
  'FUE',
  'PFO',
  'ACE',
  'MLA',
  'TFS',
  'BHX',
  'LPL',
  'IBZ',
  'PMO',
  'BDS',
  'ALC',
  'RIX',
  'TLL',
  'KGS',
  'KUN',
  'BRI',
  'PIK',
  'EMA',
  'BRS',
  'LTN',
  'MAN',
  'NCE',
  'LGW',
  'BVA',
  'BZR',
  'VXO',
  'LBA',
  'LPP',
  'NCL',
  'CAG',
  'CRL',
  'BRU',
  'NTE',
  'VOL',
  'TPS',
  'CTA',
  'BOH',
  'MST',
  'CUF',
  'XCR',
  'HAM',
  'EBU',
  'RNS',
  'GPA',
  'VLL',
  'PUY',
  'SXF',
  'ATH',
  'PEG',
  'SZZ',
  'MJV',
  'CIY',
  'PIS',
  'AAR',
  'PGF',
  'GLA',
  'KIR',
  'XRY',
  'TGD',
  'FMO',
  'LIL',
  'TNG',
  'MAH',
  'FCO',
  'DLE',
  'FNI',
  'FSC',
  'DNR',
  'BVE',
  'RBA',
  'BIQ',
  'LDE',
  'MPL',
  'GRQ',
  'CRV',
  'TFN',
  'CFE',
  'CWL',
  'BES',
  'BSL',
  'MMX',
  'TLN',
  'LEJ',
  'RJK',
  'FEZ',
  'NDR',
  'ZTH',
  'SZG',
  'LUZ',
  'VST',
  'PDV' ];

function main() {
  client.search({
    index: 'ryantrip_current',
    body: {
      size: 99999
    }
  }).then(function (results) {
    var flights = results.hits.hits.map(function (x) { return x._source; });
    return calculateItinerary(flights, new Date(), process.argv[2]);
  }).then(function (itinerary) {
    itinerary.forEach(function (x) {
      console.log(x.outbound.airportFrom.iataCode + ' -> ' + x.outbound.airportTo.iataCode + ', ' + x.outbound.dateFrom + ', ' + x.outbound.price.value + x.outbound.price.currencySymbol);
    });
    client.close()
  });
}

main();
